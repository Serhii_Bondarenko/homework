// const squareSize = +prompt('Enter height');
//
//
// const createSquare = (squareSize) => {
//     const div = document.querySelector("body")
//     const square = document.createElement('div');
//     square.style.cssText = `
//           background: red;
//           width: ${squareSize}px;
//           height: ${squareSize}px; `
//
//     div.append(square);
// }
//
// createSquare(squareSize);

// const newSquare = createSquare(squareSize);

//Клонирование элемента -> cloneNode(false); cloneNode(true);

// console.log("header", header); //header == secondHeader

// const newHeader = header.cloneNode(false);
//
// // console.log("newHeader", newHeader); //header !== newHeader
//
// const secondHeader = header.cloneNode(true);
///////////////////////////////////////////////////////////////////////
// console.log("secondHeader", secondHeader);

//Document Fragment -> фрагмент документа -> обертка для html элементов страницы

// let fragment = new DocumentFragment(); //<> </>
//
//
// const peoples = [
//     {name: "Artem", age: 23},
//     {name: "Artem", age: 12},
//     {name: "Artem", age: 2},
//     {name: "Artem", age: 23},
//     {name: "Artem", age: 23},
//     {name: "Artem", age: 3},
//     {name: "Artem", age: 23},
//     {name: "Artem", age: 3},
//     {name: "Artem", age: 23},
//     {name: "Artem", age: 2443},
//     {name: "Artem", age: 123},
//     {name: "Artem", age: 23},
//     {name: "Artem", age: 23},
//     {name: "Artem", age: 23},
//     {name: "Artem", age: 23},
//     {name: "Artem", age: 23}
// ];

// for (let i = 0; i < peoples.length; i++) {
//     let p = document.createElement("p");
//     let li = document.createElement("li");
//     p.append(`${peoples[i].name}`);
//     li.append(`${peoples[i].age}`);
//
//
//     fragment.append(p);
//     fragment.append(li);
// }
//
// console.log("before unpacking", fragment);
//
// header.prepend(fragment);
//
// console.log("after unpacking", fragment);
//////////////////////////////////////////////////////////////////////////////////
// Написать небольшой список задач (to do list)

// Спрашивать у пользователя пункты для добавления в список до тех пор, пока
// он не нажмет отмена. Каждый текст введенный в prompt это новый элемент списка.
// Все пункты нужно вставить на страницу.
// Не забываем про семантику (список должен быть оформлен через ul);

//optional
// По клику на элемент списка - удалить этот пункт.
// Подсказка
// // elem.onclick = function () {
//     пишем что происходит по клику
//   };

// const delItem = (item) => item.remove();
// ;
//
// const toDo = ()=>{
//     const ul = document.createElement('ul');
//     const body = document.querySelector('body')
//     body.append(ul)
//     let itemList = prompt("Что нужно сделать?");
//
//     while (itemList !== null){
//         const li = document.createElement('li');
//         li.innerText = itemList;
//         li.onclick = ()=>delItem(li)
//         ul.append(li)
//         itemList =prompt("Что нужно сделать?");
//
//     }
//
//
// }
//
// toDo()

// /** Написать функцию fillChessBoard, которая развернёт «шахматную» доску 8 на 8.
//  *
//  * Цвет тёмных ячеек — #161619.
//  * Цвет светлых ячеек — #FFFFFF.
//  * Остальные стили CSS для доски и ячеек готовы.
//  *
//  * Доску необходимо развернуть внутри элемента с классом .board.
//  *
//  * Каждая ячейка доски представляет элемент div с классом .cell.
//  *
//  * */
//
//
// const LIGHT_CELL = '#ffffff';
// const DARK_CELL = '#161619';
// const V_CELLS = 8;
// const H_CELLS = 8;
//
// const board = document.createElement('section');
//
// board.classList.add('board');
//
// const main = document.getElementsByTagName('main')[0];
//
// main.append(board);
//
// const fillChessBoard = () => {
//     const board = document.querySelector(".board");
//     for(let i = 0; i < H_CELLS; i++){
//         for (let j = 0; j < V_CELLS; j++){
//         const div = document.createElement('div');
//         div.classList.add("cell") ;
//         div.style.backgroundColor = (i + j) % 2 === 0 ? DARK_CELL : LIGHT_CELL;
//
//         board.append(div)
//     }
//     }
// }
// fillChessBoard ()

// Сделать функцию светофор, которая "зажигает" красный, желтый,
// зеленый цвет в html-разметке в зависимости от переданного аргумента (выбранный цвет)
// Все остальные будут серого цвета.
// colors =["red", "green", "yellow" ]
// const chooseColor =(color)=>{
//     const circles = document.querySelectorAll('.circle');
//     circles.forEach(item => {
//         if(!item.classList.contains(color)) item.style.backgroundColor = "grey"
//
//     })
// }
//
// chooseColor("blue")
//
/////////////////////////////////////////////////////////
// const colors =["green", "yellow", "red"];
//
// const chooseColor =(color)=>{
//     const circles = document.querySelectorAll('.circle');
//     circles.forEach(item => {
//         if(!item.classList.contains(color)) item.classList.add("grey")
//     })
// }
//
// const svetoforWork = ()=>{
//     setInterval(()=> {
//         for(let i = 0; i < colors.length; i++ ){
//             const greyArr = document.querySelectorAll(".grey");
//             greyArr.forEach(item => item.classList.remove('grey'))
//             setTimeout(() => chooseColor(colors[i]), i*1000)
//         }
//     },3000)
//
// }

// svetoforWork()

/////////////////////////////////////////////////////////////////////////
// const chooseColor = (color) => {
//   const circles = document.querySelectorAll(".circle");
//
//   circles.forEach((circle) => {
//     console.log(circle.innerText)
//     if(circle.innerText === color) {
//       circle.style.cssText =`
//         background-color: ${color};
//       `
//     }
//   })
// }

// chooseColor("red");
// chooseColor("green");
// chooseColor("yellow");

////////////////////////////////////////////////////////////////////////////

// console.log(document.getElementById("svetofor").children);
//
// const chooseColor = (color) => {
//     const circles = document.querySelectorAll(".circle");
//
//     circles.forEach((circle) => {
//
//         // console.log(circle.classList.value.includes(color));
//         console.log(circle.className.includes(color));
//         if(!circle.className.includes(color)) {
//             circle.style.cssText =`
//         background-color: grey;
//       `
//         }
//     })
// }
// // chooseColor("red");
//
// chooseColor("green");
////////////////////////////////////////////////////////////////////

