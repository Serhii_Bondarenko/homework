// #1
// const paragraphs = document.querySelectorAll('p');
// paragraphs.forEach( el => el.style.backgroundColor = "#ff0000")


// #2
// const optionsItem = document.querySelector('#optionsList');
// const optionsItemParent = optionsItem.parentElement;
// console.log('optionsItemParent', optionsItemParent);

// const optionsItemChildren = Array.from(optionsItem.children);
// if (!optionsItemChildren.length){
//     console.log('There is no child')
// }else{optionsItemChildren.forEach( function (element){
//     console.log('Options Item Child Type', element.nodeType)
// })
// }


// #3
// const testParagraph = document.querySelector('#testParagraph');
// testParagraph.innerText = 'This is a paragraph';


// #4
// const mainHeader = document.querySelector('.main-header');
// const mainHeaderChildren = mainHeader.children;
// console.log ('main-header children', mainHeaderChildren);
// const childrenHeader = Array.from(mainHeaderChildren);
// childrenHeader.forEach ((element) => element.classList.add('nav-item'))

// #5
// const removeTitle = document.querySelectorAll('.section-title');
// removeTitle.forEach((element) => element.classList.remove('section-title'));



